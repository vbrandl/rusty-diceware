## v0.3.6

* No, really, fix tests. (Added the `Word` trait)

## v0.3.5

* Fix tests. (Was broken due to a changed ThreadRng output)

## v0.3.4

* Deduplicate word printing code by using the trait `Word` and moving
  that code into a a generic function which uses that trait.
* Move to 2021 edition of Rustlang.
* Add a commented out wordlist command line option for a future
  version.
* Remove some commented out code.
* Use the `Self` keyword in `impl`s instead of the type's concrete
  name.
* Add this CHANGELOG.
